import Vue from 'vue';
import Vuex from 'vuex';
import cart from './modules/cart';
import variations from './modules/variations';
import currencies from './modules/currencies';
import discounts from './modules/discounts';
import profiles from './modules/profiles';
import colors from './modules/colors';
import glass from './modules/glass';
import accessories from './modules/accessories';

Vue.use(Vuex);

const storeModerator = store => {
  store.subscribe((mutation, state) => {
    if (mutation.type.indexOf('cart') !== -1) {
      window.localStorage.setItem(
        'cart_contents',
        JSON.stringify(state.cart.contents)
      );
    }
  });
};

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  state: {
    debug: debug,
    started: true
  },
  modules: {
    cart,
    currencies,
    discounts,
    profiles,
    variations,
    colors,
    glass,
    accessories
  },
  strict: debug,
  plugins: [storeModerator]
});
