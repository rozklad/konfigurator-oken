import axios from 'axios';

// initial state
const state = {
  colors: [],
  front: null,
  back: null
};

// getters
const getters = {};

// actions
const actions = {
  loadColors({ commit }, callback) {
    axios
      .get(
        'http://konfigadmin.rozklad.me/wizard/public/storage/colors/colors.json'
      )
      .then(response => {
        commit('updateColors', response.data);
        if (typeof callback === 'function') {
          callback();
        }
      });
  },
  selectColor({ commit }, { color, face }) {
    commit('selectColor', color, face);
  }
};

// mutations
const mutations = {
  updateColors(state, colors) {
    state.colors = colors;
  },
  selectColor(state, { color, face }) {
    state[face] = color;
  },
  resetColors(state) {
    state.colors = [];
    state.front = null;
    state.back = null;
  },
  setColors(state, options) {
    state.colors = options.colors;
    state.front = options.front;
    state.back = options.back;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
