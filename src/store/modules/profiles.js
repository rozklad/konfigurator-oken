import axios from 'axios';

// initial state
const state = {
  profiles: [],
  selected: null
};

// getters
const getters = {};

// actions
const actions = {
  loadProfiles({ commit }, callback) {
    axios
      .get(
        'http://konfigadmin.rozklad.me/wizard/public/storage/helper/assets/profiles.json'
      )
      .then(response => {
        commit('updateProfiles', response.data);
        if (typeof callback === 'function') {
          callback();
        }
      });
  },
  selectProfile({ commit }, profile) {
    commit('selectProfile', profile);
  }
};

// mutations
const mutations = {
  updateProfiles(state, profiles) {
    state.profiles = profiles;
  },
  selectProfile(state, profile) {
    state.selected = profile;
  },
  resetProfiles(state) {
    state.selected = null;
  },
  setProfiles(state, profile) {
    state.selected = profile;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
