import axios from 'axios';

// initial state
const state = {
  glass: [],
  selected: null
};

// getters
const getters = {};

// actions
const actions = {
  loadGlass({ commit }, callback) {
    axios
      .get(
        'http://konfigadmin.rozklad.me/wizard/public/storage/glass/glass.json'
      )
      .then(response => {
        commit('updateGlass', response.data);
        if (typeof callback === 'function') {
          callback();
        }
      });
  },
  selectGlass({ commit }, glass) {
    commit('selectGlass', glass);
  }
};

// mutations
const mutations = {
  updateGlass(state, glass) {
    state.glass = glass;
  },
  selectGlass(state, glass) {
    state.selected = glass;
  },
  resetGlass(state) {
    state.selected = null;
  },
  setGlass(state, glass) {
    state.selected = glass;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
