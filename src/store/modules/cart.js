import { getField, updateField } from 'vuex-map-fields';

// Local storage data
const userCartContents = JSON.parse(
  window.localStorage.getItem('cart_contents')
);

// Item layout
const defaultItemLayout = {
  default: true,
  size: {
    width: '',
    height: ''
  },
  variation: {
    width: '',
    height: '',
    type: '',
    price: ''
  },
  orientation: 'none',
  windowfaces: {
    back: null,
    front: null
  },
  accessories: {
    inner: null,
    outer: null,
    blind: null,
    handle: null
  },
  profile: null,
  glass: null
};

if (userCartContents) {
  userCartContents.map(item => {
    return { ...defaultItemLayout, ...item };
  });
}

// initial state
const state = {
  contents: userCartContents ? userCartContents || [] : [],
  currentIndex: 0,
  item: defaultItemLayout
};

// getters
const getters = {
  currentCartItem() {
    // If there is no item, return default
    return state.contents[state.currentIndex];
  },
  getCurrentCartField(state) {
    return getField(state.contents[state.currentIndex]);
  }
};

// actions
const actions = {
  addDefaultItem({ state, commit }) {
    const item = JSON.parse(JSON.stringify(state.item));
    item.default = false;
    commit('addCartItem', item);
  }
};

// mutations
const mutations = {
  addCartItem(state, item) {
    state.contents.push(item);
  },
  updateCurrentCartField(state, field) {
    updateField(state.contents[state.currentIndex], field);
  },
  setCurrentIndex(state, index) {
    state.currentIndex = parseInt(index, 10);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
