import axios from 'axios';

// initial state
const state = {
  variations: [],
  loaded: false
};

// getters
const getters = {};

// actions
const actions = {
  loadVariations({ commit }) {
    axios
      .get(
        'http://konfigadmin.rozklad.me/wizard/public/storage/helper/assets/all.json'
      )
      .then(response => {
        commit('updateVariations', response.data);
        commit('setVariationsLoaded');
      });
  }
};

// mutations
const mutations = {
  updateVariations(state, variations) {
    if (typeof variations === 'object') {
      state.variations = Object.keys(variations).map(key => variations[key]);
    } else {
      state.variations = variations;
    }
  },
  setVariationsLoaded(state) {
    state.loaded = true;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
