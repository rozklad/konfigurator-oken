import axios from 'axios';

// initial state
const state = {
  accessories: {
    inner: null,
    outer: null,
    blind: null,
    handle: null
  },
  products: {
    inner: null,
    outer: null,
    blind: null,
    handle: null
  }
};

// getters
const getters = {};

// actions
const actions = {
  selectAccessory({ commit }, { accessory, type }) {
    commit('selectAccessory', { accessory, type });
  },
  loadAccessories({ commit }, { type, callback }) {
    axios
      .get(
        `http://konfigadmin.rozklad.me/wizard/public/storage/accessories/${type}.json`
      )
      .then(response => {
        commit('updateAccessories', { data: response.data, type });
        if (typeof callback === 'function') {
          callback();
        }
      });
  }
};

// mutations
const mutations = {
  selectAccessory(state, { accessory, type }) {
    state.accessories[type] = accessory;
  },
  updateAccessories(state, { data, type }) {
    state.products[type] = data;
  },
  resetAccessories(state) {
    state.accessories = {
      inner: null,
      outer: null,
      blind: null,
      handle: null
    };
  },
  setAccessories(state, accessories) {
    state.accessories = accessories;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
