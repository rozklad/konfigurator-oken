import axios from 'axios';

// initial state
const state = {
  currencies: []
};

// getters
const getters = {};

// actions
const actions = {
  loadCurrencies({ commit }) {
    axios
      .get('http://konfigadmin.rozklad.me/wizard/public/storage/currencies.php')
      .then(response => {
        commit('updateCurrencies', response.data);
      });
  }
};

// mutations
const mutations = {
  updateCurrencies(state, currencies) {
    state.currencies = currencies;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
