import Vue from 'vue';
import vuexI18n from 'vuex-i18n';
import App from './App.vue';
import store from './store';
import router from './router';
import { mapGetters, mapState } from 'vuex';

Vue.config.productionTip = false;

Vue.use(vuexI18n.plugin, store);

// Localization
const translationsCs = {
  remove: 'Odebrat',
  headerTitle: 'Sklad oken',
  headerHeadline: 'Kalkulátor oken a dveří',
  heightTooltip: 'Výška se udává bez podokenního profilu 30mm.',
  confirmSize: 'Potvrdit rozměry',
  widthLabel: 'Šířka',
  widthHelp: '({min} - {max} mm)',
  heightLabel: 'Výška',
  heightHelp: '({min} - {max} mm)',
  lengthLabel: 'Délka',
  lengthHelp: '({min} - {max} mm)',
  depthLabel: 'Hloubka',
  depthHelp: '({min} - {max} mm)',
  sizeTitle: 'Zadejte požadované rozměry',
  variationTitle: 'Vyberte vhodnou variantu pro zadané rozměry',
  variationSize: '{width} &times; {height} mm',
  priceNoVat: '{value} Kč bez DPH',
  priceNoVatPlus: '<strong>+ {value} Kč</strong> bez DPH',
  priceNoVatStrong: '<strong>{value} Kč</strong> bez DPH',
  confirmVariation: 'Potvrdit výběr',
  orientation: 'Otevírání',
  orientationLeftSymbol: 'Levé',
  orientationRightSymbol: 'Pravé',
  summaryLabelName: 'Název:',
  summaryLabelWidth: 'Šířka:',
  summaryLabelHeight: 'Výška:',
  summaryLabelOrientation: 'Otevírání:',
  summary: {
    value: {
      left: 'Levé',
      right: 'Pravé',
      none: 'Fixní (neotvíratelné)'
    }
  },
  changeVariation: 'Upravit výběr',
  stepNum: 'Krok {number}',
  profileTitle: 'Vyberte požadovaný profil',
  propustnost: 'Koeficient prostupu tepla',
  hloubka: 'Stavební hloubka',
  komory: 'Počet komor',
  profileTooltipLabel: 'Kompletní info o profilu',
  profileInfoTitle: 'Kompletní info o profilu',
  accessoryInfoTitle: 'Kompletní informace k doplňku',
  colorTitle: 'Vyberte barvu a dekor',
  profileLabel: 'Profil:',
  color: {
    title: {
      front: 'Vnitřní pohled',
      back: 'Venkovní pohled'
    },
    label: {
      front: 'Vnitřní barva:',
      back: 'Venkovní barva:'
    },
    front: {
      title: 'Vyberte vnitřní barvu'
    },
    back: {
      title: 'Vyberte vnější barvu'
    },
    choose: {
      front: 'Vybrat vnitřní barvu',
      back: 'Vybrat venkovní barvu'
    },
    edit: {
      front: 'Upravit vnitřní barvu',
      back: 'Upravit venkovní barvu'
    },
    picker: {
      confirm: 'Potvrdit barvu'
    }
  },
  glass: {
    title: 'Vyberte sklo'
  },
  accessories: {
    title: 'Vyberte doplňky',
    button: 'Vybrat doplněk',
    button_change: 'Upravit doplněk',
    inner: 'Vnitřní plastový parapet',
    outer: 'Vnější hliníkový parapet',
    blind: 'Hliníkové žaluzie',
    handle: 'Hliníkové kliky',
    property: {
      length: 'Délka: {value} mm',
      depth: 'Hloubka: {value} mm',
      width: 'Šířka: {value} mm',
      height: 'Výška: {value} mm',
      covers: 'Krytky: +{value} Kč bez DPH'
    },
    shorts: {
      inner: 'Vnitřní parapet:',
      outer: 'Vnější parapet:',
      blind: 'Žaluzie:',
      handle: 'Klika:',
      inner_covers: 'Vnitřní parapet - krytky',
      outer_covers: 'Vnější parapet - krytky'
    }
  },
  accessoryTooltipLabel: 'Kompletní informace k doplňku',
  glassLabel: 'Sklo:',
  orderTitle: 'Rekapitulace a objednávka',
  table: {
    head: {
      productNumber: 'Číslo výrobku',
      image: 'Obrázek',
      description: 'Popis výrobku',
      itemPrice: 'Položková cena [bez DPH]',
      count: 'Počet',
      price: 'Cena celkem [bez DPH]'
    },
    body: {
      productNumber: 'Výrobek #{value}'
    }
  },
  orderButton: 'Objednat',
  addAnother: 'Přidat další výrobek',
  edit: 'Upravit',
  variationTooltip:
    /* '1/ P = pravé otevírání, L = levé otevírání. <br>2/ */
    'Počet kusů kalkulovaného výrobku si definujete v posledním<br>kroku kalkulace “Rekapitulace a objednávka”'
};

Vue.i18n.add('cs', translationsCs);
Vue.i18n.set('cs');

Vue.mixin({
  computed: {
    ...mapState(['started']),
    ...mapState('cart', ['contents']),
    ...mapGetters({
      currentCartItem: 'cart/currentCartItem'
    }),
    discounts() {
      return this.$store.state.discounts;
    },
    currencies() {
      return this.$store.state.currencies.currencies;
    },
    fullPrice() {
      let current = this.currentCartItem;
      if (typeof current === 'undefined') {
        current = {
          count: 1
        };
      }
      return (
        this.currencyFormatNum(this.getItemPrice(current)) *
        (parseInt(current.count, 10) > 10 ? parseInt(current.count, 10) : 1)
      );
    },
    fullCartPrice() {
      let sum = 0;
      this.contents.forEach(element => {
        if (element !== null) {
          sum +=
            this.currencyFormatNum(this.getItemPrice(element)) *
            (parseInt(element.count, 10) > 0 ? parseInt(element.count, 10) : 1);
        }
      });
      return sum;
    }
  },
  methods: {
    /**
     * Preview
     */
    getOkna({
      variation,
      orientation,
      width,
      height,
      canvasWidth,
      texture,
      textureImage
    }) {
      let openable = true;
      let top = false;
      const bottom = false;

      if (variation.type.indexOf('neotvíratelné') !== -1) {
        openable = false;
      }
      if (
        (variation.type.indexOf('výklopné') !== -1 ||
          variation.type.indexOf('sklopné') !== -1 ||
          variation.type.indexOf('sklopně posuvné') !== -1) &&
        orientation !== 'none'
      ) {
        top = true;
        if (variation.type.indexOf('otvíravé') === -1) {
          openable = false;
        }
      }
      let localHeight = parseInt(height, 10);
      let ratio = 1;
      let localWidth = parseInt(width, 10);
      if (width >= canvasWidth) {
        localWidth = canvasWidth;
        ratio = width / localWidth;
      } else if (width < canvasWidth) {
        localWidth = canvasWidth;
        ratio = localWidth / width;
      }
      localHeight = height * ratio;

      let localTexture = texture;
      if (typeof texture !== 'undefined') {
        localTexture = texture;
      }

      const window = {
        sirka_ramu: 14, // px
        startX: 0,
        startY: 0,
        sirka: parseInt(localWidth, 10), // mm
        vyska: parseInt(localHeight, 10), // mm
        otvirani: {
          top: top ? 1 : 0,
          right: orientation === 'right' && openable ? 1 : 0,
          bottom: bottom ? 1 : 0,
          left: orientation === 'left' && openable ? 1 : 0
        },
        pocetMriziVertikalnich: 0,
        pocetMriziHorizontalnich: 0,
        sirkaMrizi: 4, // px
        sitka: 1, // 0/1
        zaluzie: 0, // 0/1
        pricka: 0, // 0/1
        texture: localTexture,
        textureImage
      };

      // Double
      if (variation.type.indexOf('dvoukřídlé') !== -1) {
        const single = JSON.parse(JSON.stringify(window));
        single.sirka /= 2;
        const first = JSON.parse(JSON.stringify(single));
        first.startX = 0;
        single.startX = single.sirka / 2;
        if (
          variation.type.indexOf('se středovým sloupkem') !== -1 ||
          variation.type.indexOf('s pevným středovým sloupkem') !== -1 ||
          variation.type.indexOf('bez středového sloupku') !== -1
        ) {
          single.otvirani.right =
            single.otvirani.right === 1 || orientation === 'none' ? 0 : 1;
          single.otvirani.left =
            single.otvirani.left === 1 || orientation === 'none' ? 0 : 1;
        }
        return [first, single];
      }
      // Triple
      if (variation.type.indexOf('trojkřídlé') !== -1) {
        const single = JSON.parse(JSON.stringify(window));
        single.sirka /= 3;
        const first = JSON.parse(JSON.stringify(single));
        first.startX = 0;
        const second = JSON.parse(JSON.stringify(single));
        second.startX = first.sirka;
        single.startX = first.sirka * 2;
        return [first, second, single];
      }
      return [window];
    },
    hasOrientation(variation) {
      return !(
        parseInt(variation.typenum, 10) === 0 ||
        parseInt(variation.typenum, 10) === 1
      );
    },
    hasHandle(variation) {
      return !(parseInt(variation.typenum, 10) === 0);
    },
    /**
     * Currency
     */
    currencyFormat(number) {
      return `${this.currencyFormatNum(number)} Kč`;
    },
    currencyFormatNum(number) {
      return `${Math.ceil(parseFloat(number))}`;
    },
    /**
     * Money rules
     */
    countWithVat(input) {
      return input * 1.21;
    },
    countVat(input) {
      return input * 0.21;
    },
    getVariationPrice(variation) {
      let basePrice = 0;
      // Special rules
      if (variation.typenum === 6) {
        basePrice =
          variation && variation.price
            ? this.getNumber(
                this.getInCzk(
                  this.applyDiscount(variation.price, 'variations'),
                  'EUR'
                ) + this.getInCzk(19.76, 'EUR')
              )
            : 0;
      } else if (variation.typenum === 7) {
        basePrice =
          variation && variation.price
            ? this.getNumber(
                this.getInCzk(
                  this.applyDiscount(variation.price, 'variations'),
                  'EUR'
                ) +
                  this.getInCzk(19.76, 'EUR') +
                  this.getInCzk(170, 'PLN')
              )
            : 0;
      } else if (variation.typenum === 8) {
        basePrice =
          variation && variation.price
            ? this.getNumber(
                this.getInCzk(
                  this.applyDiscount(variation.price, 'variations'),
                  'EUR'
                ) + this.getInCzk(19.76, 'EUR')
              )
            : 0;
      } else {
        basePrice =
          variation && variation.price
            ? this.getNumber(
                this.getInCzk(
                  this.applyDiscount(variation.price, 'variations'),
                  'EUR'
                )
              )
            : 0;
      }
      return basePrice;
    },
    applyDiscount(price, type) {
      let discount = 0;
      if (typeof this.discounts[type] !== 'undefined') {
        discount = parseFloat(this.discounts[type]);
      }
      const ratio = (100 - discount) * 0.01;
      return price * ratio;
    },
    getInCzk(value, currencyCode) {
      const currency = this.currencies.find(
        element => element.currency === currencyCode
      );
      if (currency && currency.rate) {
        return (
          parseFloat(String(value).replace(/,/, '.')) *
          parseFloat(String(currency.rate).replace(/,/, '.'))
        );
      }
      return 0;
    },
    getNumberOfHandles(item) {
      let numberOfHandles = 1;

      // If no item is selected
      if (!item) {
        return numberOfHandles;
      }

      // If no variation is selected
      if (!item.variation) {
        return numberOfHandles;
      }

      if (item.variation.typenum === 0) {
        numberOfHandles = 0;
      } else if (item.variation.typenum === 1) {
        numberOfHandles = 1;
      } else if (item.variation.typenum === 4) {
        numberOfHandles = 2;
      } else if (item.variation.typenum === 6) {
        numberOfHandles = 3;
      } else if (item.variation.typenum === 7) {
        numberOfHandles = 2;
      } else if (item.variation.typenum === 11) {
        numberOfHandles = 2;
      }
      return numberOfHandles;
    },
    getHandlePrice(item) {
      if (
        item &&
        item.accessories &&
        item.accessories.handle &&
        item.accessories.handle.color &&
        item.accessories.handle.color.price
      ) {
        const numberOfHandles = this.getNumberOfHandles(item);
        return (
          this.getInCzk(
            this.applyDiscount(
              item.accessories.handle.color.price,
              'variations'
            ),
            'EUR'
          ) * numberOfHandles
        );
      }
      return 0;
    },
    getItemPrice(cartItem, quantity) {
      if (typeof cartItem === 'undefined' || cartItem === null) {
        return 0;
      }
      const basePrice = this.getVariationPrice(cartItem.variation);
      const profilePrice =
        cartItem.profile && cartItem.profile.price
          ? this.applyDiscount(this.getNumber(cartItem.profile.price), 'glass')
          : 0;
      const frontPrice =
        cartItem.windowfaces && cartItem.windowfaces.front
          ? this.getDecorationPrice(cartItem, 'front')
          : 0;
      const backPrice =
        cartItem.windowfaces && cartItem.windowfaces.back
          ? this.getDecorationPrice(cartItem, 'back')
          : 0;
      const innerPrice =
        cartItem.accessories &&
        cartItem.accessories.inner &&
        cartItem.accessories.inner.color &&
        cartItem.accessories.inner.color.price
          ? this.applyDiscount(cartItem.accessories.inner.color.price, 'glass')
          : 0;
      const outerPrice =
        cartItem.accessories &&
        cartItem.accessories.outer &&
        cartItem.accessories.outer.color &&
        cartItem.accessories.outer.color.price
          ? cartItem.accessories.outer.color.price
          : 0;
      const blindPrice =
        cartItem.accessories &&
        cartItem.accessories.blind &&
        cartItem.accessories.blind.color &&
        cartItem.accessories.blind.color.price
          ? this.getInCzk(
              this.applyDiscount(
                cartItem.accessories.blind.color.price,
                'variations'
              ),
              'EUR'
            )
          : 0;
      const handlePrice = this.getHandlePrice(cartItem);
      const innerHandlePrice =
        cartItem.accessories &&
        cartItem.accessories.inner &&
        cartItem.accessories.inner.color &&
        cartItem.accessories.inner.color.covers
          ? cartItem.accessories.inner.color.covers
          : 0;
      const outerHandlePrice =
        cartItem.accessories &&
        cartItem.accessories.outer &&
        cartItem.accessories.outer.color &&
        cartItem.accessories.outer.color.covers
          ? cartItem.accessories.outer.color.covers
          : 0;
      const glassPrice =
        cartItem.glass && cartItem.glass.price
          ? this.applyDiscount(
              this.getInCzk(
                this.getGlassPriceOfItem(
                  cartItem.variation,
                  cartItem.glass.price
                ),
                'EUR'
              ),
              'glass'
            )
          : 0;

      const fullPrice =
        basePrice +
        profilePrice +
        Math.ceil(frontPrice) +
        Math.ceil(backPrice) +
        innerPrice +
        outerPrice +
        blindPrice +
        handlePrice +
        innerHandlePrice +
        outerHandlePrice +
        glassPrice;

      /*
      console.log(
        basePrice,
        profilePrice,
        frontPrice,
        backPrice,
        innerPrice,
        outerPrice,
        blindPrice,
        handlePrice,
        innerHandlePrice,
        outerHandlePrice,
        glassPrice,
      );
      */

      return fullPrice * (quantity || 1);
    },
    getCurrentDecorationPrice(decoration, face) {
      let ratio = 0;
      let type = 'inner';
      if (face === 'front') {
        type = 'inner';
      } else if (face === 'back') {
        type = 'outer';
      }
      if (decoration && decoration[`${type}_ratio`]) {
        ratio = decoration[`${type}_ratio`] * 0.01;
      }

      return this.variation && this.variation.price
        ? this.getNumber(
            this.getInCzk(
              this.applyDiscount(this.variation.price, 'variations'),
              'EUR'
            )
          ) * ratio
        : 0;
    },
    getDecorationPrice(item, face) {
      let ratio = 0;
      let type = 'inner';
      if (face === 'front') {
        type = 'inner';
      } else if (face === 'back') {
        type = 'outer';
      }
      if (
        item.windowfaces &&
        item.windowfaces[face] &&
        item.windowfaces[face][`${type}_ratio`]
      ) {
        ratio = item.windowfaces[face][`${type}_ratio`] * 0.01;
      }

      return item.variation && item.variation.price
        ? this.getNumber(
            this.getInCzk(
              this.applyDiscount(item.variation.price, 'variations'),
              'EUR'
            )
          ) * ratio
        : 0;
    },
    getGlassPriceOfItem(variation, pricePer) {
      const widthInMeters = variation.width / 1000;
      const heightInMeters = variation.height / 1000;
      const size = widthInMeters * heightInMeters;
      return this.applyDiscount(size * pricePer, 'glass');
    },
    getGlassPrice(pricePer) {
      return this.getGlassPriceOfItem(this.currentCartItem.variation, pricePer);
    },
    /**
     * Numbers
     */
    propertyLimit(collection, property, type) {
      if (typeof collection !== 'undefined' && collection !== null) {
        const item = collection.reduce((prev, curr) => {
          if (type === 'min') {
            return prev[property] < curr[property] ? prev : curr;
          }
          return prev[property] > curr[property] ? prev : curr;
        });
        if (item && item[property]) {
          return item[property];
        }
      }
      return 0;
    },
    roundToUpperHundred(value) {
      return Math.ceil(value / 100) * 100;
    },
    roundToHundred(value) {
      return Math.floor(value / 100) * 100;
    },
    getNumber(number) {
      return Math.ceil(parseFloat(String(number).replace(/,/, '.')));
    },
    /**
     * URL
     */
    getStorageUrl(url) {
      if (url.indexOf('http') !== -1) {
        return url;
      }
      return url.replace(
        '/static/',
        'http://konfigadmin.rozklad.me/wizard/public/storage/'
      );
    }
  }
});

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
