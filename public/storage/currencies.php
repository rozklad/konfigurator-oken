<?php
/**
 * Konfigurator
 *
 * @package  Rozklad/Konfigurator
 * @author   bitterend <info@bitterend.io>
 * @version  1.0.1
 */

/*
|--------------------------------------------------------------------------
| Load currencies
|--------------------------------------------------------------------------
|
| Load current currency rates from CNB
|
*/

// Load data
$url = 'https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date=' . date('d.m.Y');
$contents = file_get_contents($url);

// Define rows
$rows = explode("\n", $contents);

// Prepare output
$output = [];

// Extract information from rows
foreach ($rows as &$row)
{
  $parts = explode('|', $row);
  if (isset($parts[3]) && isset($parts[4]))
  {
    $output[] = [
      'currency' => $parts[3],
      'rate' => $parts[4],
    ];
  }
}

// Setup headers
header('Content-Type: application/json');

// Print json
echo json_encode($output);
